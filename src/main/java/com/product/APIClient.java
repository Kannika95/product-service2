package com.product;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;



@FeignClient(value="file-upload",url = "http://localhost:8083")
public interface APIClient {
	
	@GetMapping(value = "/files/getFiles")
	ResponseEntity<List<ResponseFile>> getListFiles();
	
	@PostMapping(value = "/files/upload")
	ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file);
	
}
