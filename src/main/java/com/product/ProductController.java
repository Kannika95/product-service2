package com.product;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import reactor.core.publisher.Flux;


@RestController
@RequestMapping("/api/product")
public class ProductController {
	
	
	@Autowired
    ProductRepository productRepository;
	
	@Autowired
    private RestTemplate restTemplate;
//	
	@Autowired
    private WebClient webClient;
	
	@Autowired
	private APIClient apiClient;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @CircuitBreaker(name = "product-service", fallbackMethod = "getProductFiles")
    public List<Product> findAll() {
    	 ResponseEntity<List<ResponseFile>> template = restTemplate.exchange("http://localhost:8083/files/getFiles", HttpMethod.GET, null,
    			new ParameterizedTypeReference<List<ResponseFile>>(){});
    	 List<ResponseFile> temp=template.getBody();
    	System.out.println("commun 2"+temp);
//    	
//     Flux<List<ResponseFile>> client = webClient.get()
//        .uri("http://localhost:8083/files/getFiles")
//                .retrieve()
//                        .bodyToFlux(new ParameterizedTypeReference<List<ResponseFile>>(){});
// 	System.out.println(client);
// 	WebClient vbndsvfdsnbfdsf = WebClient.create();
// 	System.out.println(vbndsvfdsnbfdsf);
// 	WebClient string = WebClient.create("http://localhost:9092");
// 	System.out.println(string);
//    	ResponseEntity<List<ResponseFile>> rytrytrytrytr = apiClient.getListFiles();
//    	System.out.println(rytrytrytrytr);
        return productRepository.findAll();
    }
    
    public List<ResponseFile> getProductFiles(Exception e){
		return new ArrayList<>();
    	
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createProduct(@RequestParam("file") MultipartFile file) {
    	ResponseEntity<ResponseMessage> erererer = apiClient.uploadFile(file);
    	System.out.println(erererer);
//        productRepository.save(product);
    }
	
    @GetMapping("/hello")
    public String hello() {
    	System.out.println("Service 2 called");
    	return "Hello world";
    }

}
